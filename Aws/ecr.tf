provider "aws" {
  region = "us-east-1"
}


resource "aws_ecr_repository" "ecr" {
  name                 = "cloudguardregistry"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

data "aws_ecr_authorization_token" "token" {
}

variable "gitlab_access_token"{
    type = string
}
 
provider "gitlab" {
    token = var.gitlab_access_token
}
 
data "gitlab_project" "example_project" {
    id = 30945345
}
 
resource "gitlab_project_variable" "sample_project_variable" {
    project = data.gitlab_project.example_project.id
    key = "DOCKER_USER"
    value = data.aws_ecr_authorization_token.token.user_name
}

resource "gitlab_project_variable" "sample_project_variable2" {
    project = data.gitlab_project.example_project.id
    key = "DOCKER_PASSWD"
    value = data.aws_ecr_authorization_token.token.password
}

resource "gitlab_project_variable" "sample_project_variable3" {
    project = data.gitlab_project.example_project.id
    key = "registry"
    value = aws_ecr_repository.ecr.repository_url
}
