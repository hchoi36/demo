resource gitlab_project_cluster "bar" {
  project                       = 30945345
  name                          = "CloudGuard-EKS"
  enabled                       = true
  kubernetes_api_url            = module.eks.cluster_endpoint
  kubernetes_token              = data.kubernetes_secret.gitlab-admin-token.data.token
  kubernetes_ca_cert            = trimspace(base64decode(data.aws_eks_cluster.my-cluster.certificate_authority.0.data))
  kubernetes_authorization_type = "rbac"
  environment_scope             = "staging"
}
