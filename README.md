# Gitlab CI/CD pipeline with Azure
## Getting Started
These instructions will help to build a CI/CD pipeline that is able to build the code into an image and then 

### **Prerequisites** 
- Have a Azure account and create a ubuntu 20.04 Linux machine size: Standard D2s v3 (2 vcpus, 8 GiB memory) with premium ssd or your preference
    - Make sure the network security group Inbound port rule allowed port 80 for docker container access
    - Use this command to gain access to VM ssh -i <private key path> azureuser@azure_ip , gain access to ubuntu with pem key or use putty. 

#### **Docker installation**
Here is the instructions to download Docker on the ubuntu virtual machine [Docker_installation](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)

## **Gitlab yml file instructions:**
[gitlab_ymlfile](https://docs.gitlab.com/ee/ci/yaml/)

## **Dockerfile:**
Dockerfile instructions: [Dockerfile_instructions](https://docs.docker.com/engine/reference/builder/)

Changed dockerfile from COPY. ./ to add ./v1 and added nginx.config to add all files in folder v1 and config file to the container: [ref](https://linuxtut.com/en/f259af4c9e86596b4a91/)

## **Azure Terraform:**
https://docs.gitlab.com/ee/user/infrastructure/iac/
- Watch video for gitlab variable creation 
- Added Gitlab personal access token TF_VAR_gitlab_access_token
- Changed project id to ours

https://about.gitlab.com/topics/gitops/gitlab-enables-infrastructure-as-code/
- Used this for Kubernetes set up and creation in azure  

https://docs.convox.com/installation/production-rack/azure/
- Used this to get ARM credentials (Used to connect Gitlab to Azure)
- az account set --subscription="$ARM_SUBSCRIPTION_ID" for this $ARM_SUB replace with ur id 
    - Issue when creating service principal
    - Make sure the display name is UNIQUE (terraform -> terraform2)
NOTE: all variables must be masked in GitLab CI/CD variables in order to run terraform

https://claytonshieldsjr.medium.com/terraform-azure-gitlab-ci-94ab4653e0fc
- simplified previous with this 
- But changed hashicorp version to 1.0.0 instead of 0.12.1

https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_cluster
- Changed GitLab cluster creation method to this 
- Changed project to the project id 


## **AWS Terraform:**
https://learn.hashicorp.com/tutorials/terraform/eks
https://about.gitlab.com/blog/2020/12/15/deploy-aws/
- Get service credentials from aws settings via creating access keys 
- Then add them to gitlab environment variables

https://github.com/hashicorp/learn-terraform-provision-eks-cluster
- Example of eks terraform for reference 
- Issue with subnets & worker_groups being unsupported arguments had to change terraform version =  "17.24.0" in eks.tf

### AWS deployment
- Go to load balancer and copy DNS to display pod/web app 

## **Sonarqube:**
- sudo docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 -v sonarqube-conf:/opt/sonarqube/conf -v sonarqube-data:/opt/sonarqube/data -v sonarqube-logs:/opt/sonarqube/logs -v sonarqube-extensions:/opt/sonarqube/extensions sonarqube
    - Docker command to pull sonarqube image and run container 

- Default log in is admin | admin then you can change to any password
- Login Info => User: admin | Password: Cloudguard123
- Running on port 9000 => http://20.124.125.123:9000 
    - ip is virtual machine's public ip 
    - Able to do this by adding port 9000 for inbound rule in Azure
    - Change destination port not source port leave as-is for any or define vm ip 
    - Make sure to stop kubernetes if using. 
 
## **Dockerhub/private registry:**
- docker pull nats
- docker tag nats:latest cloudguardregistry.azurecr.io/nats:latest
- docker push cloudguardregistry.azurecr.io/nats:latest
- https://askubuntu.com/questions/1382704/docker-push-to-private-registry-an-image-does-not-exist-locally-with-the-tag
    - Make sure to tag the image to push into the private registry


## **Selenium:**
- https://cylab.be/blog/134/automated-web-application-testing-with-php-selenium-and-gitlab
- https://gist.github.com/aczietlow/7c4834f79a7afd920d8f
- https://anil.io/blog/php/using-selenium-with-php/
    - Used to set up selenium php
- https://github.com/php-webdriver/php-webdriver#getting-started
    - Used to config capability settings to disable SSL cert & make headless for less overhead

## **Selenium webdriver(facebook):**
- https://www.w3schools.com/php/default.asp
    - Php coding tutorial
    - Created custom html file for selenium report by creating file and writing in it 
- https://gist.github.com/aczietlow/7c4834f79a7afd920d8f
    - Cheatsheet for for using php webdriver (facebook/webdriver).
- https://github.com/php-webdriver/php-webdriver/blob/main/example.php
    - reference of test cases
- https://php-webdriver.github.io/php-webdriver/latest/Facebook/WebDriver/WebDriverBy.html
    - Official website for detailed commands
- NOTE: xml files and without.php in tests are not being used was failed approach 

## **Dast:**
- https://docs.gitlab.com/ee/user/application_security/dast/
- https://gitlab-doc-test.readthedocs.io/zh_CN/latest/user/application_security/dast/
    - Gitlab dast full scan takes too long 
- https://about.gitlab.com/blog/2020/08/31/how-to-configure-dast-full-scans-for-complex-web-applications/
    - Could be helpful in reducing dast full scan
- https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html
    - DAST browser-based crawler instead of dast full scan and limiting action to 10 could work instead 





