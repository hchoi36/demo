<?php

require_once __DIR__ . "/vendor/autoload.php";

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Chrome\ChromeOptions;

function setUp()
{
    $selenium_url = getenv("SELENIUM_URL");
    if ($selenium_url === false) {
        $selenium_url = "http://localhost:4444/wd/hub";
    }

    $capabilities = DesiredCapabilities::chrome();
// Disable accepting SSL certificates
    $capabilities->setCapability('acceptSslCerts', false);

// Add arguments via Chrome to start headless Chrome
    $ChromeOptions = new ChromeOptions();
    $ChromeOptions->addArguments(['-headless']);
    $capabilities->setCapability(ChromeOptions::CAPABILITY, $ChromeOptions);

    $browser = RemoteWebDriver::create(
            $selenium_url,
            $capabilities);

    return $browser;
}

function Twitter($browser) 
{
    $browser->get("https://hchoi36.github.io/");
    $el1 = $browser->findElement(WebDriverBy::id('Twitter'))->click();
    if (sizeof($el1) > 0) {
        echo "T successfully clicked"; 
        return 1;
    }
    else {
        return 0; 
    }
}

function Facebook($browser) 
{
    $browser->get("https://hchoi36.github.io/");
    $el1 = $browser->findElement(WebDriverBy::id('Facebook'))->click();
    if (sizeof($el1) > 0) {
        echo "FB successfully clicked";
        return 1;
    }
    else {
        return 0; 
    }
}

function Youtube($browser) 
{
    $browser->get("https://hchoi36.github.io/");
    $el1 = $browser->findElement(WebDriverBy::id('Youtube'))->click();
    if (sizeof($el1) > 0) {
        echo "YT successfully clicked";
        return 1;
    }
    else {
        return 0; 
    }
}

function Instagram($browser) 
{
    $browser->get("https://hchoi36.github.io/");
    $el1 = $browser->findElement(WebDriverBy::id('Instagram'))->click();
    if (sizeof($el1) > 0) {
        echo "INSTA successfully clicked";
        return 1;
    }
    else {
        return 0; 
    }
}

function Text($browser) 
{
    $passes = 0; 
    $browser->get("http://20.232.80.136/");
    //check title is correct
    if ($browser->getTitle() == 'CloudGuard'){
        $passes +=1;
    }
    //xpath to Team: CloudGuard header and check 
    if ($browser->findElement(WebDriverBy::xpath('//*[@id="about-us"]/div/h1'))->getText() == 'Team: CloudGuard'){
        $passes += 1;
    }
    //xpath to Team About Us header
    if ($browser->findElement(WebDriverBy::xpath('//*[@id="about-us"]/div/div/div[1]/div[2]/div/h3'))->getText() == 'About Us'){
        $passes += 1;
    }
    //xpath to Senior Deisgn Project header
    if ($browser->findElement(WebDriverBy::xpath('//*[@id="about-us"]/div/div/div[2]/div[1]/div/h3'))->getText() == 'Senior Deisgn Project'){
        $passes +=1;
    }
    //xpath to Project Objectives header
    if ($browser->findElement(WebDriverBy::xpath('//*[@id="about-us"]/div/div/div[3]/div[2]/div/h3'))->getText() == 'Project Objectives'){
        $passes +=1;
    }
    //xpath to Locations/Cyber-threat Map header
    if ($browser->findElement(WebDriverBy::xpath('//*[@id="our-map"]/div/h1'))->getText() == 'Locations/Cyber-threat Map'){
        $passes += 1;
    }

    if ($passes = 6) {
        return 1;
    }
    else {
        return 0; 
    }  
}

function sidenav($browser)
{
    $passes = 0; 
    $browser->get("http://20.232.80.136/");
    //checks opening side navigation bar
    if (sizeof($browser->findElement(WebDriverBy::id('open-navbar-button'))->click()) > 0){
        $passes += 1;
    }
    //checks clicking About Us tab on side nav
    if(sizeof($browser->findElement(WebDriverBy::cssSelector('#mySidenav > a:nth-child(2)'))->click()) > 0){
        $passes +=1;
    }
    //checks clicking Video tab on side nav
    if(sizeof($browser->findElement(WebDriverBy::cssSelector('#mySidenav > a:nth-child(5)'))->click()) > 0){
        $passes +=1;
    }
    //checks clicking Video tab on side nav
    if(sizeof($browser->findElement(WebDriverBy::cssSelector('#mySidenav > a:nth-child(9)'))->click()) > 0){
        $passes +=1;
    }
    if ($passes = 4){
        return 1;
    }
    else {
        return 0; 
    }
}

$browser = setUp();

$passes = Twitter($browser) + Facebook($browser) + Youtube($browser) + Instagram($browser) + Text($browser) + sidenav($browser);
$fail = 0;
if (Twitter($browser) == 0){
    $fail += 1;
}
if (Facebook($browser) == 0){
    $fail += 1;
}
if (Youtube($browser) == 0){
    $fail += 1;
}
if (Instagram($browser) == 0){
    $fail += 1;
}
if (Text($browser) == 0){
    $fail += 1;
}
if (sidenav($browser) == 0){
    $fail += 1;
}

print("\npasses:" . $passes);

$report = fopen("report.html", "w");
fwrite($report, '
<!DOCTYPE html>
<html>
<head> 
<title>Selenium Report</title> 
</head> 

<body>
<h2> <div style="margin: 20px">&nbsp; Selenium Report</div></h2>
');

fwrite($report, '<div style="width: 200px; padding: 50px; margin: 20px; height: 200px; border: 15px solid rgb(8, 138, 43)">&nbsp; Passes:' . $passes . '</div>' );
fwrite($report, '<div style="width: 200px; padding: 50px; margin: 20px; height: 200px; border: 15px solid rgb(219, 30, 30)">&nbsp; Fails:' . $fail . '</div>'); 
//fwrite($report, '<h2>Fails: ' . $fail . '</h2>');
fwrite($report, 
'
</body>
</html>');

fclose($report);

